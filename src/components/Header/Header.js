import React, {Component} from 'react';

// const Header = ({heading}) =>{
//     return(
//         <div>
//             <h1>{heading} </h1>
//             <h1>"Hello world" </h1>
//         </div>
//     );
// };

class Header extends Component{
    state={
        myvar: "",
    }
    componentDidMount(){
        this.setState({
            myvar : this.props.heading
        });
    }
    render(){
        console.log("my var is ",this.state.myvar);
        return(
            <div>
               <h1>{this.props.heading} </h1>
            </div>
        );
    }
}
export default Header;